package com.example.offerdaysongs.dto.requests;

import com.example.offerdaysongs.model.Company;
import com.example.offerdaysongs.model.Recording;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
public class CreateCopyrightRequest {
    String description;
    Recording recording;
    Company company;
    ZonedDateTime startTime;
    ZonedDateTime endTime;
    BigDecimal price;
}
