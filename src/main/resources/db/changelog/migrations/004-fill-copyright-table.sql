INSERT INTO copyright (description, recording_id, company_id, start_time, end_time, price ) VALUES
    ('Copyright 1', '1', '1', '2015-01-15', '2018-01-15', 30000);

INSERT INTO copyright (description, recording_id, company_id, start_time, end_time, price ) VALUES
    ('Copyright 2', '2', '3', '2010-01-15', '2016-01-15', 40000);

INSERT INTO copyright (description, recording_id, company_id, start_time, end_time, price ) VALUES
    ('Copyright 3', '3', '2', '2018-02-15', '2022-01-15', 40000);

INSERT INTO copyright (description, recording_id, company_id, start_time, end_time, price ) VALUES
    ('Copyright 4', '4', '4', '20119-02-15', '2025-01-15', 50000);