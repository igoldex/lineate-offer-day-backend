package com.example.offerdaysongs.repository;

import com.example.offerdaysongs.model.Copyright;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;

@Repository
public interface CopyrightRepository extends JpaRepository<Copyright, Long>, JpaSpecificationExecutor<Copyright> {

    @Query("SELECT c FROM Copyright c WHERE c.startTime >= :startPeriod AND c.endTime <= :endPeriod ")
     List<Copyright> findCopyrightsByPeriod(@Param("startPeriod") ZonedDateTime startTime, @Param("endPeriod")ZonedDateTime endTime);

     List<Copyright> findByCompanyId(long companyId);
}
