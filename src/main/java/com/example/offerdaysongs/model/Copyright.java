package com.example.offerdaysongs.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@Entity
public class Copyright {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String description;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="recording_id")
    Recording recording;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="company_id")
    Company company;
    ZonedDateTime startTime;
    ZonedDateTime endTime;
    BigDecimal price;
}
