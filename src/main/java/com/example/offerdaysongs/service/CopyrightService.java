package com.example.offerdaysongs.service;

import com.example.offerdaysongs.dto.CopyrightDto;
import com.example.offerdaysongs.dto.requests.CreateCompanyRequest;
import com.example.offerdaysongs.dto.requests.CreateCopyrightRequest;
import com.example.offerdaysongs.dto.requests.CreateRecordingRequest;
import com.example.offerdaysongs.model.Company;
import com.example.offerdaysongs.model.Copyright;
import com.example.offerdaysongs.model.Recording;
import com.example.offerdaysongs.repository.CompanyRepository;
import com.example.offerdaysongs.repository.CopyrightRepository;
import com.example.offerdaysongs.repository.RecordingRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class CopyrightService {

    private final CopyrightRepository copyrightRepository;
    private final RecordingRepository recordingRepository;
    private final CompanyRepository companyRepository;
    private final RecordingService recordingService;
    private final CompanyService companyService;

    final static DateTimeFormatter formatter
            = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Autowired
    ModelMapper modelMapper;

    public CopyrightService(CopyrightRepository copyrightRepository,
                            RecordingRepository recordingRepository,
                            CompanyRepository companyRepository,
                            RecordingService recordingService,
                            CompanyService companyService) {
        this.copyrightRepository = copyrightRepository;
        this.recordingRepository = recordingRepository;
        this.companyRepository = companyRepository;
        this.recordingService = recordingService;
        this.companyService = companyService;
    }

    public List<Copyright> getAll() {
        return copyrightRepository.findAll();
    }

    public Copyright getById(long id) {
        return copyrightRepository.getById(id);
    }

    @Transactional
    public Copyright create(CreateCopyrightRequest request) {
        Copyright copyright = new Copyright();
        copyright.setDescription(request.getDescription());
        copyright.setStartTime(request.getStartTime());
        copyright.setEndTime(request.getEndTime());
        copyright.setPrice(request.getPrice());
        var recordingDto = request.getRecording();
        if (recordingDto != null) {
            var recording = recordingRepository.findById(recordingDto.getId()).orElseGet(() ->
                    recordingService.create(convertToRecordingRequest(recordingDto)));
            copyright.setRecording(recording);
        }
        var companyDto = request.getCompany();
        if (companyDto != null) {
            var company = companyRepository.findById(companyDto.getId()).orElseGet(() ->
                   companyService.create(convertToCompanyRequest(companyDto)));
            copyright.setCompany(company);
        }
        return copyrightRepository.save(copyright);
    }

    public List<Copyright> getCopyrightsByPeriod(String startTime, String endTime) {
        ZonedDateTime formattedStartTime= getZonedDatetime(startTime);
        ZonedDateTime formattedEndTime =getZonedDatetime(endTime);
        return copyrightRepository.findCopyrightsByPeriod(formattedStartTime, formattedEndTime);
    }

    public List<Copyright> getCopyrightByCompany(long companyId) {
        return copyrightRepository.findByCompanyId(companyId);
    }

    private ZonedDateTime getZonedDatetime(String dateTime) {
        LocalDate date = LocalDate.parse(dateTime);
        return date.atStartOfDay(ZoneId.systemDefault());
    }

    private CreateRecordingRequest convertToRecordingRequest(Recording recording) {

        CreateRecordingRequest createRecordingRequest = new CreateRecordingRequest();
        createRecordingRequest.setTitle(recording.getTitle());
        createRecordingRequest.setVersion(recording.getVersion());
        createRecordingRequest.setReleaseTime(recording.getReleaseTime());
        createRecordingRequest.setSinger(recording.getSinger());

        return createRecordingRequest;
    }

    private CreateCompanyRequest convertToCompanyRequest(Company company) {
        CreateCompanyRequest createCompanyRequest = new CreateCompanyRequest();
        createCompanyRequest.setName(company.getName());

        return createCompanyRequest;
    }


    public Copyright update(CopyrightDto copyrightDto) throws ParseException {
        return copyrightRepository.save(convertToEntity(copyrightDto));
    }

    private Copyright convertToEntity(CopyrightDto copyrightDto) throws ParseException {
        return modelMapper.map(copyrightDto, Copyright.class);
    }

}
