CREATE TABLE copyright
(
    id              BIGSERIAL PRIMARY KEY,
    description     VARCHAR(4096),
    recording_id    BIGINT REFERENCES recording (id),
    company_id      BIGINT REFERENCES company (id),
    start_time      TIMESTAMP,
    end_time        TIMESTAMP,
    price           NUMERIC
);