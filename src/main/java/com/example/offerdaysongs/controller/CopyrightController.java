package com.example.offerdaysongs.controller;

import com.example.offerdaysongs.dto.CompanyDto;
import com.example.offerdaysongs.dto.CopyrightDto;
import com.example.offerdaysongs.dto.RecordingDto;
import com.example.offerdaysongs.dto.SingerDto;
import com.example.offerdaysongs.dto.requests.CreateCopyrightRequest;
import com.example.offerdaysongs.dto.requests.CreateRecordingRequest;
import com.example.offerdaysongs.model.Company;
import com.example.offerdaysongs.model.Copyright;
import com.example.offerdaysongs.model.Recording;
import com.example.offerdaysongs.service.CopyrightService;
import com.example.offerdaysongs.service.RecordingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/copyrights")
public class CopyrightController {

    private static final String ID = "id";
    private final CopyrightService copyrightService;
    @Autowired
    private RecordingController recordingController;

    public CopyrightController(CopyrightService copyrightService) {
        this.copyrightService = copyrightService;
    }

    @GetMapping("/")
    public List<CopyrightDto> getAll(){
        return copyrightService.getAll().stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id:[\\d]+}")
    public CopyrightDto get(@PathVariable(ID) long id) {
        var recording = copyrightService.getById(id);
        return convertToDto(recording);
    }

    @PostMapping("/")
    public CopyrightDto create(@RequestBody CreateCopyrightRequest request) {
        return convertToDto(copyrightService.create(request));
    }

    @PutMapping("/{id:[\\d]+}/edit")
    public CopyrightDto update(@PathVariable(ID) long id, @RequestBody CopyrightDto copyrightDto) throws ParseException {
        if(copyrightDto.getId() != id) {
            copyrightDto.setId(id);
        }
        return convertToDto(copyrightService.update(copyrightDto));
    }

    @GetMapping("/period")
    public List<CopyrightDto> getCopyrightByPeriod(@RequestParam String startTime, String endTime) {
        List<Copyright> copyrights = copyrightService.getCopyrightsByPeriod(startTime, endTime);
        List<CopyrightDto> copyrightDtoList = new ArrayList<>();

        for(Copyright copyright : copyrights) {
            copyrightDtoList.add(convertToDto(copyright));
        }
        return copyrightDtoList;
    }

    @GetMapping("/bycompany/{id:[\\d]+}")
    public List<CopyrightDto> getCopyrightByCompany(@PathVariable(ID) long companyId) {
        List<Copyright> copyrights = copyrightService.getCopyrightByCompany(companyId);
        List<CopyrightDto> copyrightDtoList = new ArrayList<>();

        for(Copyright copyright : copyrights) {
            copyrightDtoList.add(convertToDto(copyright));
        }
        return copyrightDtoList;
    }

    private CopyrightDto convertToDto(Copyright copyright) {
        var recording = copyright.getRecording();
        var company = copyright.getCompany();

        return new CopyrightDto(copyright.getId(),
                                copyright.getDescription(),
                                convertToRecordingDto(recording),
                                convertToCompanyDto(company),
                                copyright.getStartTime(),
                                copyright.getEndTime(),
                                copyright.getPrice());
    }

    private RecordingDto convertToRecordingDto(Recording recording) {
        var singer = recording.getSinger();
        return recording != null ? new RecordingDto(recording.getId(),
                recording.getTitle(),
                recording.getVersion(),
                recording.getReleaseTime(),
                singer != null ? new SingerDto(singer.getId(), singer.getName()) : null) : null;
    }

    private CompanyDto convertToCompanyDto(Company company) {
        return company != null ? new CompanyDto(company.getId(), company.getName()) : null;
    }
}
