package com.example.offerdaysongs.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
public class CopyrightDto {
    long id;
    String Description;
    RecordingDto recording;
    CompanyDto company;
    ZonedDateTime startTime;
    ZonedDateTime endTime;
    BigDecimal price;
}
